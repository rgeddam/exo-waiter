package com.nespresso.exercise.waiter;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;

public class Restaurant {
	private Order order;

    public int initTable(int sizeOfTable) {
    	int tableNum = sizeOfTable;
    	order = new Order(sizeOfTable);
    	return tableNum;
    }

    public void customerSays(int tableId, String message) {
        int flag = 0;
    	String splitMessage[] = message.split(":");
    	String person = splitMessage[0].trim();
    	String dish = splitMessage[1].trim();

    	int numOfPersons = order.orderDetails.size();
    	int count= 0;
    	if(order.orderDetails.size() >= 1){ 
    		//Checking for person already exists
    		for(int i=0; i<numOfPersons; i++){
    			if(person.equals(order.orderDetails.get(i).getPerson())){
    				order.set(i, person, dish, i);
    				flag = 1;
    			}
    		}
    		////////////////////////////////////////// 		
    		//Start of Checking for Same
    		//////////////////////////////////////////
    		if(flag != 1){
				if(dish.equalsIgnoreCase("same")){
					dish = order.orderDetails.get(numOfPersons-1).getDish();
					order.add(tableId, person, dish);			
				}else{
					order.add(tableId, person, dish);				
				}
    		}
    		//////////////////////////////////////////
    		//End of Checking for Same
			
    	}else{
    		order.add(tableId, person, dish);  		
    	}
    	//throw new UnsupportedOperationException();
    }

    public String createOrder(int tableId) {
    	
    	String orderInformation = "";
    	int numOfPersons = order.orderDetails.size();
    	if(order.getTableSize() != numOfPersons)
    	{
    		String msg = "MISSING ";
    		msg += order.getTableSize() - numOfPersons;
    		return msg;
    	}
    	
    	if(order.tableSize == numOfPersons){
    		int numOfFishDishes = 0;
    		for(int i=0; i<numOfPersons; i++){
    			if(("Fish for 2").equalsIgnoreCase(order.orderDetails.get(i).getDish())){
    				numOfFishDishes++;
    			}
    		}
    		
    		if(numOfFishDishes == 1){
    			String msg = "MISSING 1 for Fish for 2";
        		return msg;
    		}
    	}
    	
    	for(int i=0; i<numOfPersons; i++){
    		
    		orderInformation += order.orderDetails.get(i).getDish();
    		if(i != numOfPersons-1){
    		 orderInformation += ", ";
    		}
    	}
    	
    	return orderInformation;
    }
    
    public static void log(String message){
    	System.out.println(message);
    }
    
    public static void main(String args[]){
    	final Restaurant restaurant = new Restaurant();
        final int tableId = restaurant.initTable(4);
    }
    
    private class Order{
    	private String person;
    	private String dish;
    	private int tableSize;
    	public ArrayList<Order> orderDetails = new ArrayList<Order>();
    	private int tableId;
    	
    	public Order(int tableSize)
    	{
    		this.tableSize = tableSize;
    	}
		public ArrayList<Order> getOrderDetails() {
			return orderDetails;
		}

		public void setOrderDetails(ArrayList<Order> orderDetails) {
			this.orderDetails = orderDetails;
		}

		public String getPerson() {
			return person;
		}
		
		public void setPerson(String person) {
			this.person = person;
		}
		
		public String getDish() {
			return dish;
		}
		
		public void setDish(String dish) {
			this.dish = dish;
		}
		
		public int getTableSize() {
			return tableSize;
		}
    	
    	public void add(int myTtableId, String person, String dish){
    		Order order = new Order(myTtableId);
    		order.setPerson(person);
    		order.setDish(dish);
    		order.tableId = myTtableId;
    		orderDetails.add(order);
    	}
      	public void set(int myTtableId, String person, String dish, int index){
    		Order order = new Order(myTtableId);
    		order.setPerson(person);
    		order.setDish(dish);
    		order.tableId = myTtableId;
    		orderDetails.set(index, order);
    	}    	
    }
}
